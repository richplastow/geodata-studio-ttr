# FabricPlanet

An interactive [geodata.studio](https://geodata.studio) infographic for [The Textile Review](https://thetextilereview.com)

### TTR Brand Guidelines
- Purple: #3d0333
- Light Green: #d2e4d8
