import * as THREE from "three";
import React from "react";
import ReactDOM from "react-dom";
import { Canvas } from "react-three-fiber";

const log = msg => (document.querySelector("#log").innerHTML += msg + "\n");

function Thing({ vertices }) {
  return (
    <group ref={ref => log("we have access to the instance")}>
      <line>
        <geometry
          attach="geometry"
          vertices={vertices.map(v => new THREE.Vector3(...v))}
          onUpdate={self => (self.verticesNeedUpdate = true)}
        />
        <lineBasicMaterial attach="material" color="black" />
      </line>
      <mesh
        onClick={e => log("click")}
        onPointerOver={e => log("hover")}
        onPointerOut={e => log("unhover")}
      >
        <octahedronGeometry attach="geometry" />
        <meshBasicMaterial
          attach="material"
          color="peachpuff"
          opacity={0.5}
          transparent
        />
      </mesh>
    </group>
  );
}

ReactDOM.render(
  <Canvas>
    <Thing
      vertices={[[-1, 0, 0], [0, 1, 0], [1, 0, 0], [0, -1, 0], [-1, 0, 0]]}
    />
  </Canvas>,
  document.getElementById("root")
);
